#!/usr/bin/fish

rm -f ~/.tmux.conf
rm -fr ~/.config
rm -f ~/.emacs
rm -fr ~/.emacs.d

set path (status filename | sed 's/[^\/]*$//')

ln -s $PWD/.config ~/.config
ln -s $PWD/.emacs.d ~/.emacs.d
ln -s $PWD/.tmux.conf ~/.tmux.conf

emacs --script ~/.emacs.d/installpackages.el
