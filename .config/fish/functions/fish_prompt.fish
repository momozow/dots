function fish_prompt --description 'Write out the prompt'
    set -l home_escaped (echo -n $HOME | sed 's/\//\\\\\//g')
    set -l pwd (echo -n $PWD | sed "s/^$home_escaped/~/" | sed 's/ /%20/g')

    if test (tmux ls &>/dev/null; echo $status) -eq 1
        set -g tmux_sessions
    else if test -z "$TMUX"
        set -g tmux_sessions (set_color cyan) (tmux ls -F "[#S]" | xargs)
    end

    printf "\n%s%s%s%s%s" (set_color $fish_color_cwd) $pwd (echo -n $tmux_sessions) (set_color yellow) (__fish_git_prompt)

    set_color normal
    set prompt_symbol '$'
    printf "\n%s " $prompt_symbol
end
