function fish_user_key_bindings
    bind \cd delete-char
end

if test (tmux ls 2> /dev/null | wc -l) -eq 0 # no session
    tmux new-session -s auto
else if test (env | grep -c TMUX) -eq 0 && test (tmux ls | grep -c auto) -eq 1 # not on a session && there is auto session
    tmux attach -t auto
end

set -x LANG en_US.utf8
set -x BAT_THEME TwoDark

abbr -a ls exa -a
abbr -a ll exa -al
abbr -a cat bat
