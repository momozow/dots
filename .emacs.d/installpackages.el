;;; MELPA
(defvar my-favorite-package-list
  '(company
    docker-compose-mode
    dockerfile-mode
    ess
    fish-mode
    flycheck
    magit
    org
    rjsx-mode
    undo-tree
    undohist
    web-mode)
  "packages to be installed")

(require 'package)
(setq package-user-dir "~/.emacs.d/elisp/")
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)
(unless package-archive-contents (package-refresh-contents))
(dolist (pkg my-favorite-package-list)
  (unless (package-installed-p pkg)
    (package-install pkg)))
