;; Calendar
(setq system-time-locale "C") ; change DOW to english
(setq calendar-holidays nil) ; disable general holidays
(setq calendar-week-start-day 1) ; monday start

;; key-bind
(require 'org)
(define-key org-mode-map (kbd "C-c C-x C-s") nil)
(global-set-key (kbd "C-c a") 'org-agenda)
(setq org-agenda-files '("~/org/org.org"))

(setq org-agenda-clockreport-parameter-plist
      (quote (:maxlevel 3 :lang "en" :scope file :block nil :wstart 1 :mstart 1 :tstart nil :tend nil :step nil :stepskip0 nil :fileskip0 nil :tags nil :emphasize nil :link nil :narrow 40! :indent t :formula % :timestamp nil :level nil :tcolumns nil :formatter nil)))
(setq org-agenda-todo-list-sublevels 2)
(setq org-agenda-use-time-grid nil)

(setq org-time-clocksum-format '(:hours "%d" :require-hours t :minutes ":%02d" :require-minutes t))

;; TODO status
(setq org-todo-keywords
      '((sequence "TODO(t)" "WAIT(w)" "|" "DONE(d)")
        (sequence "SOMEDAY(s)" "REGULAR(r)" "|")))
;; TODO color
(setq org-todo-keyword-faces
      '(("TODO" . "#FF2A46")
        ("WAIT" . "#D4FF2A")
        ("DONE" . "#46FF2A")
        ("SOMEDAY" . "#2A3FFF")
        ("REGULAR" . "#2A3FFF")
        ))

(put 'set-goal-column 'disabled nil)

(require 'ox-latex)
(setq org-latex-pdf-process
      '("platex %f"
        "platex %f"
        "dvipdfmx %b.dvi"
        "rm %f %b.dvi"))
(setq org-latex-with-hyperref nil)
(add-to-list 'org-latex-classes
             '("simple"
               "\\documentclass{jsarticle}
                [NO-PACKAGES]
                [NO-DEFAULT-PACKAGES]"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\section{%s}" . "\\section*{%s}")
               ("\\section{%s}" . "\\section*{%s}")
               ("\\section{%s}" . "\\section*{%s}")
               ("\\section{%s}" . "\\section*{%s}")))
(setq org-latex-default-class "simple")


(setq org-plantuml-jar-path "~/.emacs.d/lib/plantuml.jar")
(org-babel-do-load-languages
 'org-babel-load-languages
 '((plantuml . t)))
