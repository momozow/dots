(require 'package)
(setq package-user-dir "~/.emacs.d/elisp/")
(package-initialize)

(require 'undohist)
(undohist-initialize)

(require 'undo-tree)
(global-undo-tree-mode)

(setq backup-inhibited t)
(setq make-backup-files nil)
(setq delete-auto-save-files t)

(setq load-path (append '("~/.emacs.d/conf") load-path))
(load "C-init")
(load "org-init")

(setq-default indent-tabs-mode nil)
(setq-default show-trailing-whitespace t)
(setq delete-auto-save-files t); when exit
(show-paren-mode 1); highlight brackets

(global-set-key "\C-h" 'delete-backward-char);; C-h to delete
(global-set-key "\C-m" 'newline-and-indent);; return and Auto Indent
